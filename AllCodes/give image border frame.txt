// border frame using a function
function setBlack(px){
    px.setRed(0);
    px.setGreen(0);
    px.setBlue(0);
    return px;
}
var image = new SimpleImage("lion.jpg");
for (var pixel of image.values()) {
    var x = pixel.getX();
    var y = pixel.getY();
    if (x < 10){
        pixel = setBlack(pixel);
    }
    if (y < 10){
        pixel = setBlack(pixel);
    }
    if (x >= image.getWidth()-10){
        pixel = setBlack(pixel);
    }
    if (y >= image.getHeight()-10){
        pixel = setBlack(pixel);
    }
}
print(image);
